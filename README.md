# BeePM密码管理器
依赖JDK8，请自行安装。

# 截图
![](./image/0.png)
![](./image/1.png)
![](./image/2.png)

# 许可 License
AGPL v3
任何二次开发及二次开源项目请严格遵守相应开源许可

# 捐助 Donate
本软件为作者业余时间编写，如果该软件对您有所帮助，希望能捐助作者以优化本软件。

![](./image/alipay.png)