package com.github.leesean07.pm.kit;

import javafx.scene.input.Clipboard;
import javafx.scene.input.ClipboardContent;

public class ClipboardKit {
    public static void set(String s) {
        Clipboard clipboard = Clipboard.getSystemClipboard();
        ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.putString(s);
        clipboard.setContent(clipboardContent);
    }
}
