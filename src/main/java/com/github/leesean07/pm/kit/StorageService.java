package com.github.leesean07.pm.kit;

import java.util.List;

public interface StorageService {
    public void storage(List<PasswordItem> list) throws Exception;

    public List<PasswordItem> load();
}
