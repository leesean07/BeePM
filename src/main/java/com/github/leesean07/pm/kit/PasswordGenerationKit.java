package com.github.leesean07.pm.kit;

import java.util.Random;

public class PasswordGenerationKit {
    private static char[] SPECIAL_CHAR_ARRAY = {'!', '@', '#', '$', '%', '^', '&', '*'};

    public synchronized static String get(int length, boolean regulationForCase, boolean regulationForNumber, boolean regulationForSpecialChar) {
        // 0 lowercase, 1 uppercase, 2 number, 3 special char
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; ) {
            int type = random.nextInt(4);
            if (type == 3 && regulationForSpecialChar) {
                sb.append(SPECIAL_CHAR_ARRAY[random.nextInt(SPECIAL_CHAR_ARRAY.length)]);
                i++;
            } else if (type == 2 && regulationForNumber) {
                sb.append((char) (random.nextInt(10) + 48));
                i++;
            } else if (type == 1 && regulationForCase) {
                sb.append((char) (random.nextInt(26) + 65));
                i++;
            } else if (type == 0) {
                sb.append((char) (random.nextInt(26) + 97));
                i++;
            }
        }
        return sb.toString();
    }
}
