package com.github.leesean07.pm.kit;

import java.io.File;

public class AppProperty {
    private String appTitle;
    private String appLogo;
    private double mainWidth;
    private double mainHeight;
    private String mainStyle;
    private String storageDir;

    public AppProperty() {
        appTitle = "BeePM - 密码管理器";
        appLogo = "/image/logo.png";
        mainWidth = 350;
        mainHeight = 560;
        mainStyle = "/css/default.css";
        File path = new File(getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
        path = path.getParentFile();
        if (path.getName().contains("lib")) {
            path = path.getParentFile();
        }
        storageDir = path.getAbsolutePath() + File.separator + "data";
    }

    public String getAppTitle() {
        return appTitle;
    }

    public void setAppTitle(String appTitle) {
        this.appTitle = appTitle;
    }

    public double getMainWidth() {
        return mainWidth;
    }

    public void setMainWidth(double mainWidth) {
        this.mainWidth = mainWidth;
    }

    public double getMainHeight() {
        return mainHeight;
    }

    public void setMainHeight(double mainHeight) {
        this.mainHeight = mainHeight;
    }

    public String getAppLogo() {
        return appLogo;
    }

    public void setAppLogo(String appLogo) {
        this.appLogo = appLogo;
    }

    public String getMainStyle() {
        return mainStyle;
    }

    public void setMainStyle(String mainStyle) {
        this.mainStyle = mainStyle;
    }

    public String getStorageDir() {
        return storageDir;
    }

    public void setStorageDir(String storageDir) {
        this.storageDir = storageDir;
    }
}
