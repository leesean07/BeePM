package com.github.leesean07.pm.kit;

import com.google.common.io.Files;
import com.google.common.primitives.Bytes;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

public class DefaultStorageServiceV1 implements StorageService {
    public final static byte VERSION = 1;
    public final static int KEY_SIZE = 16;

    @Override
    public void storage(List<PasswordItem> list) throws Exception {
        char empty = 0;
        char vs = 1;
        char hs = 2;
        File dir = prepareDir();
        backup(dir);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            PasswordItem item = list.get(i);
            if (i != 0) {
                sb.append(vs);
            }
            sb.append(item.getId()).append(hs)
                    .append(item.getName()).append(hs)
                    .append(item.getAccount()).append(hs)
                    .append(item.getPassword()).append(hs)
                    .append(item.getPasswordLength()).append(hs)
                    .append(item.isPasswordRegulationForCase()).append(hs)
                    .append(item.isPasswordRegulationForNumber()).append(hs)
                    .append(item.isPasswordRegulationForSpecialChar()).append(hs)
                    .append(item.getRemark().equals("") ? empty : item.getRemark()).append(hs)
                    .append(item.getIconColor()).append(hs);
        }
        byte[] data = Bytes.concat(new byte[]{VERSION}, sb.toString().getBytes("UTF-8"));
        Files.write(data, getDataFile(dir));
    }

    @Override
    public List<PasswordItem> load() {
        File dir = prepareDir();
        List<PasswordItem> list = null;
        try {
            File dataFile = getDataFile(dir);
            if (!dataFile.exists()) {
                return list;
            }
            byte[] data = Files.toByteArray(dataFile);
            if (data.length < KEY_SIZE + 1) {
                return list;
            }
            byte version = data[0];
            if (version != VERSION) {
                return list;
            }
            byte[] temp = new byte[data.length - 1];
            System.arraycopy(data, 1, temp, 0, data.length - 1);
            String s = new String(data, "UTF-8");
            char empty = 0;
            char vs = 1;
            char hs = 2;
            String[] sa = s.split(String.valueOf(vs));
            list = new LinkedList<>();
            for (int i = 0; i < sa.length; i++) {
                String[] item = sa[i].split(String.valueOf(hs));
                if (item.length >= 10) {
                    PasswordItem passwordItem = new PasswordItem();
                    passwordItem.setId(Long.valueOf(item[0]));
                    passwordItem.setName(item[1]);
                    passwordItem.setAccount(item[2]);
                    passwordItem.setPassword(item[3]);
                    passwordItem.setPasswordLength(Integer.valueOf(item[4]));
                    passwordItem.setPasswordRegulationForCase(Boolean.valueOf(item[5]));
                    passwordItem.setPasswordRegulationForNumber(Boolean.valueOf(item[6]));
                    passwordItem.setPasswordRegulationForSpecialChar(Boolean.valueOf(item[7]));
                    passwordItem.setRemark(item[8].equals(String.valueOf(empty)) ? "" : item[8]);
                    passwordItem.setIconColor(item[9]);
                    list.add(passwordItem);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    private File prepareDir() {
        File dir = new File(BeanManager.getAppProperty().getStorageDir());
        if (dir.exists() && dir.isFile()) {
            dir.delete();
        }
        if (!dir.exists()) {
            dir.mkdirs();
        }
        return dir;
    }

    private void backup(File dir) {
        File data = getDataFile(dir);
        File backup = getBackupFile(dir);
        if (backup.exists()) {
            backup.delete();
        }
        if (data.exists()) {
            data.renameTo(backup);
        }
    }

    private File getDataFile(File dir) {
        return new File(dir.getPath() + File.separator + "data");
    }

    private File getBackupFile(File dir) {
        return new File(dir.getPath() + File.separator + "backup");
    }
}
