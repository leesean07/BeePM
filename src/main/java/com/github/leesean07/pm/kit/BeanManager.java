package com.github.leesean07.pm.kit;

import com.github.leesean07.pm.controller.DetailViewController;
import com.github.leesean07.pm.controller.ListViewController;
import com.github.leesean07.pm.controller.MainController;

public class BeanManager {
    private static AppProperty APP_PROPERTY;
    private static StorageService STORAGE_SERVICE;
    private static MainController MAIN_CONTROLLER;
    private static ListViewController LIST_VIEW_CONTROLLER;
    private static DetailViewController DETAIL_VIEW_CONTROLLER;

    public synchronized static AppProperty getAppProperty() {
        if (APP_PROPERTY == null) {
            APP_PROPERTY = new AppProperty();
        }
        return APP_PROPERTY;
    }

    public synchronized static StorageService getStorageService() {
        if (STORAGE_SERVICE == null) {
            STORAGE_SERVICE = new DefaultStorageServiceV1();
        }
        return STORAGE_SERVICE;
    }

    public synchronized static MainController getMainController() {
        if (MAIN_CONTROLLER == null) {
            MAIN_CONTROLLER = new MainController();
        }
        return MAIN_CONTROLLER;
    }

    public synchronized static ListViewController getListViewController() {
        if (LIST_VIEW_CONTROLLER == null) {
            LIST_VIEW_CONTROLLER = new ListViewController();
        }
        return LIST_VIEW_CONTROLLER;
    }

    public synchronized static DetailViewController getDetailViewController() {
        if (DETAIL_VIEW_CONTROLLER == null) {
            DETAIL_VIEW_CONTROLLER = new DetailViewController();
        }
        return DETAIL_VIEW_CONTROLLER;
    }
}
