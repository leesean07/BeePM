package com.github.leesean07.pm.kit;

import java.util.Random;

public class ColorRandomKit {
    private static String[] COLOR_ARRAY = {"icon-yellow", "icon-blue", "icon-green", "icon-red", "icon-gray"};

    public static String get() {
        Random random = new Random();
        int number = random.nextInt(COLOR_ARRAY.length);
        return COLOR_ARRAY[number];
    }
}
