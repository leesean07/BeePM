package com.github.leesean07.pm.kit;

import javafx.beans.property.*;

import java.util.Objects;

public class PasswordItem {
    private long id;
    private StringProperty name;
    private StringProperty account;
    private StringProperty password;
    private IntegerProperty passwordLength;
    private BooleanProperty passwordRegulationForSpecialChar;
    private BooleanProperty passwordRegulationForCase;
    private BooleanProperty passwordRegulationForNumber;
    private StringProperty remark;
    private String iconColor;
    private boolean visible;

    public PasswordItem() {
        id = System.currentTimeMillis();
        name = new SimpleStringProperty();
        name.set("默认名称");
        account = new SimpleStringProperty();
        account.set("默认账户");
        password = new SimpleStringProperty();
        password.set("");
        passwordLength = new SimpleIntegerProperty();
        passwordLength.set(6);
        passwordRegulationForSpecialChar = new SimpleBooleanProperty();
        passwordRegulationForSpecialChar.set(false);
        passwordRegulationForCase = new SimpleBooleanProperty();
        passwordRegulationForCase.set(false);
        passwordRegulationForNumber = new SimpleBooleanProperty();
        passwordRegulationForNumber.set(false);
        remark = new SimpleStringProperty();
        remark.set("");
        iconColor = ColorRandomKit.get();
        visible = true;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getAccount() {
        return account.get();
    }

    public StringProperty accountProperty() {
        return account;
    }

    public void setAccount(String account) {
        this.account.set(account);
    }

    public String getPassword() {
        return password.get();
    }

    public StringProperty passwordProperty() {
        return password;
    }

    public void setPassword(String password) {
        this.password.set(password);
    }

    public int getPasswordLength() {
        return passwordLength.get();
    }

    public IntegerProperty passwordLengthProperty() {
        return passwordLength;
    }

    public void setPasswordLength(int passwordLength) {
        this.passwordLength.set(passwordLength);
    }

    public boolean isPasswordRegulationForSpecialChar() {
        return passwordRegulationForSpecialChar.get();
    }

    public BooleanProperty passwordRegulationForSpecialCharProperty() {
        return passwordRegulationForSpecialChar;
    }

    public void setPasswordRegulationForSpecialChar(boolean passwordRegulationForSpecialChar) {
        this.passwordRegulationForSpecialChar.set(passwordRegulationForSpecialChar);
    }

    public boolean isPasswordRegulationForCase() {
        return passwordRegulationForCase.get();
    }

    public BooleanProperty passwordRegulationForCaseProperty() {
        return passwordRegulationForCase;
    }

    public void setPasswordRegulationForCase(boolean passwordRegulationForCase) {
        this.passwordRegulationForCase.set(passwordRegulationForCase);
    }

    public boolean isPasswordRegulationForNumber() {
        return passwordRegulationForNumber.get();
    }

    public BooleanProperty passwordRegulationForNumberProperty() {
        return passwordRegulationForNumber;
    }

    public void setPasswordRegulationForNumber(boolean passwordRegulationForNumber) {
        this.passwordRegulationForNumber.set(passwordRegulationForNumber);
    }

    public String getRemark() {
        return remark.get();
    }

    public StringProperty remarkProperty() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark.set(remark);
    }

    public String getIconColor() {
        return iconColor;
    }

    public void setIconColor(String iconColor) {
        this.iconColor = iconColor;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PasswordItem that = (PasswordItem) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
