package com.github.leesean07.pm.controller;

import com.github.leesean07.pm.kit.BeanManager;
import com.github.leesean07.pm.kit.PasswordItem;
import com.github.leesean07.pm.view.MainView;
import javafx.collections.ObservableList;
import javafx.scene.Node;

import java.util.List;

public class MainController extends BaseController<MainView> {
    private MainView mainView;

    public MainView initAndGetView() {
        if (mainView == null) {
            mainView = new MainView();
            initAction();
        }
        beforeAction();
        return mainView;
    }

    public void reset(Node... elements) {
        mainView.getChildren().clear();
        mainView.getChildren().addAll(elements);
    }

    protected void initAction() {
        loadData();
    }

    protected void beforeAction() {
        reset(BeanManager.getListViewController().initAndGetView());
    }

    private void loadData() {
        BeanManager.getListViewController().addPasswordItem(BeanManager.getStorageService().load());
    }
}
