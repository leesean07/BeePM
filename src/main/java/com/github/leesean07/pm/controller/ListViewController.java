package com.github.leesean07.pm.controller;

import com.github.leesean07.pm.kit.BeanManager;
import com.github.leesean07.pm.kit.ClipboardKit;
import com.github.leesean07.pm.kit.PasswordItem;
import com.github.leesean07.pm.view.ListView;
import com.google.common.base.Strings;
import javafx.beans.InvalidationListener;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.util.List;

public class ListViewController extends BaseController<ListView> {
    private ListView listView;
    private ObservableList<PasswordItem> passwordItemList;

    public ListViewController() {
        passwordItemList = FXCollections.observableArrayList();
    }

    public ListView initAndGetView() {
        if (listView == null) {
            listView = new ListView();
            initAction();
        }
        beforeAction();
        return listView;
    }

    public ObservableList<PasswordItem> getPasswordItemList() {
        return passwordItemList;
    }

    public void addPasswordItem(List<PasswordItem> list) {
        if (list == null) {
            return;
        }
        for (PasswordItem item : list) {
            addPasswordItem(item);
        }
    }

    public void addPasswordItem(PasswordItem passwordItem) {
        if (passwordItem == null) {
            return;
        }
        passwordItemList.remove(passwordItem);
        passwordItemList.add(passwordItem);
    }

    protected void initAction() {
        passwordItemList.addListener((InvalidationListener) observable -> {
            refreshList();
        });
        listView.getSearchTextField().textProperty().addListener((observable) -> {
            String prefix = listView.getSearchTextField().getText();
            for (PasswordItem item : getPasswordItemList()) {
                if (Strings.isNullOrEmpty(prefix)) {
                    item.setVisible(true);
                } else if (!item.getName().toLowerCase().startsWith(prefix.toLowerCase())) {
                    item.setVisible(false);
                } else {
                    item.setVisible(true);
                }
            }
            refreshList();
        });
        listView.getCopyAccountMenuItem().setOnAction((event) -> {
            ContextMenu contentMenu = listView.getContentMenu();
            if (contentMenu.getUserData() != null) {
                PasswordItem passwordItem = (PasswordItem) contentMenu.getUserData();
                ClipboardKit.set(passwordItem.getAccount());
            }
        });
        listView.getCopyPasswordMenuItem().setOnAction((event) -> {
            ContextMenu contentMenu = listView.getContentMenu();
            if (contentMenu.getUserData() != null) {
                PasswordItem passwordItem = (PasswordItem) contentMenu.getUserData();
                ClipboardKit.set(passwordItem.getPassword());
            }
        });
        listView.getRemoveMenuItem().setOnAction((event) -> {
            ContextMenu contentMenu = listView.getContentMenu();
            if (contentMenu.getUserData() != null) {
                PasswordItem passwordItem = (PasswordItem) contentMenu.getUserData();
                getPasswordItemList().remove(passwordItem);
                if (getPasswordItemList().size() == 0) {
                    getPasswordItemList().addAll(new PasswordItem());
                }
                try {
                    BeanManager.getStorageService().storage(getPasswordItemList());
                } catch (Exception e) {
                }
            }
        });
        listView.getAddMenuItem().setOnAction((event) -> {
            BeanManager.getMainController().reset(BeanManager.getDetailViewController().initAndGetView());
        });
        listView.getUpdateMenuItem().setOnAction((event) -> {
            ContextMenu contentMenu = listView.getContentMenu();
            if (contentMenu.getUserData() != null) {
                PasswordItem passwordItem = (PasswordItem) contentMenu.getUserData();
                DetailViewController detailViewController = BeanManager.getDetailViewController();
                detailViewController.setPasswordItem(passwordItem);
                BeanManager.getMainController().reset(detailViewController.initAndGetView());
            }
        });
    }

    protected void beforeAction() {
        if (passwordItemList.size() == 0) {
            addPasswordItem(new PasswordItem());
        }
        listView.getSearchTextField().textProperty().set("");
    }

    private void refreshList() {
        VBox vBox = new VBox();
        for (int i = 0, size = passwordItemList.size(); i < size; i++) {
            PasswordItem passwordItem = passwordItemList.get(i);
            Label iconLabel = new Label();
            iconLabel.setText(passwordItem.getName().substring(0, 1));
            iconLabel.getStyleClass().addAll("list-item-icon");
            iconLabel.getStyleClass().addAll(passwordItem.getIconColor());
            iconLabel.setAlignment(Pos.CENTER);
            iconLabel.setMinHeight(40);
            iconLabel.setMinWidth(40);
            Label accountLabel = new Label();
            accountLabel.setText(passwordItem.getName());
            accountLabel.getStyleClass().addAll("list-item-account");
            Label nameLabel = new Label();
            nameLabel.setText(passwordItem.getAccount());
            nameLabel.getStyleClass().addAll("list-item-name");
            VBox tempVBox = new VBox();
            tempVBox.getStyleClass().addAll("list-item-box");
            tempVBox.getChildren().addAll(accountLabel, nameLabel);
            HBox.setHgrow(tempVBox, Priority.ALWAYS);
            HBox hBox = new HBox();
            hBox.getStyleClass().addAll("list-item");
            hBox.getChildren().addAll(iconLabel, tempVBox);
            hBox.addEventHandler(MouseEvent.MOUSE_CLICKED, (event) -> {
                if (listView.getContentMenu().isShowing()) {
                    listView.getContentMenu().setUserData(null);
                    listView.getContentMenu().hide();
                }
                if (event.getButton().equals(MouseButton.SECONDARY)) {
                    listView.getContentMenu().setUserData(passwordItem);
                    listView.getContentMenu().show(hBox, event.getScreenX(), event.getScreenY());
                }
            });
            if (passwordItem.isVisible()) {
                vBox.getChildren().addAll(hBox);
            }
        }
        listView.getContent().setContent(vBox);
    }
}
