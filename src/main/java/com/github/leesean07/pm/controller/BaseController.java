package com.github.leesean07.pm.controller;

import javafx.scene.Node;

public abstract class BaseController<T> {
    public abstract T initAndGetView();

    public void reset(Node... elements) {
    }

    protected abstract void initAction();

    protected abstract void beforeAction();
}
