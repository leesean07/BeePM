package com.github.leesean07.pm.controller;

import com.github.leesean07.pm.kit.BeanManager;
import com.github.leesean07.pm.kit.PasswordGenerationKit;
import com.github.leesean07.pm.kit.PasswordItem;
import com.github.leesean07.pm.view.DetailView;
import com.github.leesean07.pm.view.MainView;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.scene.control.TextField;

import java.util.Collections;

public class DetailViewController extends BaseController<DetailView> {
    private DetailView detailView;
    private PasswordItem passwordItem;

    public DetailView initAndGetView() {
        if (detailView == null) {
            detailView = new DetailView();
            initAction();
        }
        beforeAction();
        return detailView;
    }

    public void setPasswordItem(PasswordItem passwordItem) {
        this.passwordItem = new PasswordItem();
        this.passwordItem.setId(passwordItem.getId());
        this.passwordItem.setName(passwordItem.getName());
        this.passwordItem.setAccount(passwordItem.getAccount());
        this.passwordItem.setPassword(passwordItem.getPassword());
        this.passwordItem.setPasswordLength(passwordItem.getPasswordLength());
        this.passwordItem.setPasswordRegulationForCase(passwordItem.isPasswordRegulationForCase());
        this.passwordItem.setPasswordRegulationForNumber(passwordItem.isPasswordRegulationForNumber());
        this.passwordItem.setPasswordRegulationForSpecialChar(passwordItem.isPasswordRegulationForSpecialChar());
    }

    protected void initAction() {
        detailView.getPasswordTextField().focusedProperty().addListener((observable, oldValue, newValue) -> {
            TextField passwordTextField = detailView.getPasswordTextField();
            StringProperty data = (StringProperty) passwordTextField.getUserData();
            String s = null;
            if (newValue) {
                s = data.get();
            } else {
                s = passwordTextField.getText();
                s = s == null ? "" : s;
                data.set(s);
                s = invisiblePassword(s);
            }
            passwordTextField.setText(s == null ? "" : s);
        });
        detailView.getGenerationButton().setOnAction((event) -> {
            String s = PasswordGenerationKit.get(
                    passwordItem.getPasswordLength(),
                    passwordItem.isPasswordRegulationForCase(),
                    passwordItem.isPasswordRegulationForNumber(),
                    passwordItem.isPasswordRegulationForSpecialChar()
            );
            passwordItem.setPassword(s);
            detailView.getPasswordTextField().setText(invisiblePassword(passwordItem.getPassword()));
            detailView.getPasswordTextField().requestFocus();
        });
        detailView.getSaveButton().setOnAction((event) -> {
            BeanManager.getListViewController().addPasswordItem(passwordItem);
            // dataUnbinding must after addPasswordItem
            dataUnbinding();
            BeanManager.getMainController().reset(BeanManager.getListViewController().initAndGetView());
            try {
                BeanManager.getStorageService().storage(BeanManager.getListViewController().getPasswordItemList());
            } catch (Exception e) {
            }
        });
        detailView.getCancelButton().setOnAction((event) -> {
            dataUnbinding();
            BeanManager.getMainController().reset(BeanManager.getListViewController().initAndGetView());
        });
    }

    protected void beforeAction() {
        dataBinding();
    }

    private void dataBinding() {
        if (passwordItem == null) {
            passwordItem = new PasswordItem();
        }
        detailView.getNameTextField().textProperty().bindBidirectional(passwordItem.nameProperty());
        detailView.getAccountTextField().textProperty().bindBidirectional(passwordItem.accountProperty());
        ((StringProperty) detailView.getPasswordTextField().getUserData()).bindBidirectional(passwordItem.passwordProperty());
        detailView.getPasswordLengthSlider().valueProperty().bindBidirectional(passwordItem.passwordLengthProperty());
        detailView.getPasswordRegulationCheckBoxForSpecialChar().selectedProperty().bindBidirectional(passwordItem.passwordRegulationForSpecialCharProperty());
        detailView.getPasswordRegulationCheckBoxForCase().selectedProperty().bindBidirectional(passwordItem.passwordRegulationForCaseProperty());
        detailView.getPasswordRegulationCheckBoxForNumber().selectedProperty().bindBidirectional(passwordItem.passwordRegulationForNumberProperty());
        detailView.getRemarkTextArea().textProperty().bindBidirectional(passwordItem.remarkProperty());

        TextField passwordTextField = detailView.getPasswordTextField();
        passwordTextField.setText(invisiblePassword(passwordItem.getPassword()));
    }

    private void dataUnbinding() {
        if (passwordItem == null) {
            return;
        }
        detailView.getNameTextField().textProperty().unbindBidirectional(passwordItem.nameProperty());
        detailView.getAccountTextField().textProperty().unbindBidirectional(passwordItem.accountProperty());
        ((StringProperty) detailView.getPasswordTextField().getUserData()).unbindBidirectional(passwordItem.passwordProperty());
        detailView.getPasswordLengthSlider().valueProperty().unbindBidirectional(passwordItem.passwordLengthProperty());
        detailView.getPasswordRegulationCheckBoxForSpecialChar().selectedProperty().unbindBidirectional(passwordItem.passwordRegulationForSpecialCharProperty());
        detailView.getPasswordRegulationCheckBoxForCase().selectedProperty().unbindBidirectional(passwordItem.passwordRegulationForCaseProperty());
        detailView.getPasswordRegulationCheckBoxForNumber().selectedProperty().unbindBidirectional(passwordItem.passwordRegulationForNumberProperty());
        detailView.getRemarkTextArea().textProperty().unbindBidirectional(passwordItem.remarkProperty());
        passwordItem = null;
    }

    private String invisiblePassword(String password) {
        return password == null ? "" : String.join("", Collections.nCopies(password.length(), "*"));
    }
}
