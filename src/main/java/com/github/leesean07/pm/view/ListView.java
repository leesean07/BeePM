package com.github.leesean07.pm.view;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class ListView extends VBox {
    private TextField searchTextField;
    private ScrollPane content;
    private ContextMenu contentMenu;
    private MenuItem copyAccountMenuItem;
    private MenuItem copyPasswordMenuItem;
    private MenuItem addMenuItem;
    private MenuItem updateMenuItem;
    private MenuItem removeMenuItem;

    public ListView() {
        // header
        searchTextField = new TextField();
        searchTextField.setMinHeight(30);
        HBox.setHgrow(searchTextField, Priority.ALWAYS);
        HBox header = new HBox();
        header.getStyleClass().addAll("main-view-header");
        header.getChildren().addAll(searchTextField);

        // content
        content = new ScrollPane();
        content.getStyleClass().addAll("main-view-content");
        content.setFitToWidth(true);
        content.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        content.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        VBox.setVgrow(content, Priority.ALWAYS);

        // menu
        copyAccountMenuItem = new MenuItem("复制账户名");
        copyPasswordMenuItem = new MenuItem("复制密码");
        addMenuItem = new MenuItem("添加");
        updateMenuItem = new MenuItem("修改");
        removeMenuItem = new MenuItem("删除");
        contentMenu = new ContextMenu();
        contentMenu.getItems().add(copyAccountMenuItem);
        contentMenu.getItems().add(copyPasswordMenuItem);
        contentMenu.getItems().add(addMenuItem);
        contentMenu.getItems().add(updateMenuItem);
        contentMenu.getItems().add(removeMenuItem);

        getChildren().addAll(header, content);
        VBox.setVgrow(this, Priority.ALWAYS);
    }

    public TextField getSearchTextField() {
        return searchTextField;
    }

    public ScrollPane getContent() {
        return content;
    }

    public ContextMenu getContentMenu() {
        return contentMenu;
    }

    public MenuItem getCopyAccountMenuItem() {
        return copyAccountMenuItem;
    }

    public MenuItem getCopyPasswordMenuItem() {
        return copyPasswordMenuItem;
    }

    public MenuItem getAddMenuItem() {
        return addMenuItem;
    }

    public MenuItem getUpdateMenuItem() {
        return updateMenuItem;
    }

    public MenuItem getRemoveMenuItem() {
        return removeMenuItem;
    }
}
