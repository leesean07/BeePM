package com.github.leesean07.pm.view;

import javafx.beans.property.SimpleStringProperty;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class DetailView extends VBox {
    private TextField nameTextField;
    private TextField accountTextField;
    private TextField passwordTextField;
    private Slider passwordLengthSlider;
    private CheckBox passwordRegulationCheckBoxForSpecialChar;
    private CheckBox passwordRegulationCheckBoxForCase;
    private CheckBox passwordRegulationCheckBoxForNumber;
    private TextArea remarkTextArea;
    private Label warningLabel;
    private Button generationButton;
    private Button saveButton;
    private Button cancelButton;

    public DetailView() {
        Label nameLabel = new Label();
        nameLabel.setText("名称");
        nameLabel.getStyleClass().add("detail-view-label");
        new VBox().setMargin(nameLabel, new Insets(0, 0, 10, 0));

        nameTextField = new TextField();
        nameTextField.getStyleClass().add("detail-view-text-field");
        new VBox().setMargin(nameTextField, new Insets(0, 0, 10, 0));

        Label accountLabel = new Label();
        accountLabel.setText("账户");
        accountLabel.getStyleClass().add("detail-view-label");
        new VBox().setMargin(accountLabel, new Insets(0, 0, 10, 0));

        accountTextField = new TextField();
        accountTextField.getStyleClass().add("detail-view-text-field");
        new VBox().setMargin(accountTextField, new Insets(0, 0, 10, 0));

        Label passwordLabel = new Label();
        passwordLabel.setText("密码");
        passwordLabel.getStyleClass().add("detail-view-label");
        new VBox().setMargin(passwordLabel, new Insets(0, 0, 10, 0));

        passwordTextField = new TextField();
        passwordTextField.getStyleClass().add("detail-view-text-field");
        // user data can not be null
        passwordTextField.setUserData(new SimpleStringProperty());
        HBox.setMargin(passwordTextField, new Insets(0, 10, 0, 0));
        HBox.setHgrow(passwordTextField, Priority.ALWAYS);
        generationButton = new Button();
        generationButton.setText("生成");
        HBox tempHBoxForPassword = new HBox();
        tempHBoxForPassword.getChildren().addAll(passwordTextField, generationButton);
        new VBox().setMargin(tempHBoxForPassword, new Insets(0, 0, 10, 0));

        Label passwordLengthLabel = new Label();
        passwordLengthLabel.setText("密码位数");
        passwordLengthLabel.getStyleClass().add("detail-view-label");
        new VBox().setMargin(passwordLengthLabel, new Insets(0, 0, 10, 0));

        passwordLengthSlider = new Slider(4, 16, 1);
        passwordLengthSlider.setShowTickMarks(true);
        passwordLengthSlider.setShowTickLabels(true);
        passwordLengthSlider.setMinorTickCount(0);
        passwordLengthSlider.setMajorTickUnit(1);
        passwordLengthSlider.setSnapToTicks(true);
        passwordLengthSlider.setValue(6);
        new VBox().setMargin(passwordLengthSlider, new Insets(0, 0, 10, 0));

        Label passwordRegulationLabel = new Label();
        passwordRegulationLabel.setText("密码规则");
        passwordRegulationLabel.getStyleClass().add("detail-view-label");
        new VBox().setMargin(passwordRegulationLabel, new Insets(0, 0, 10, 0));

        passwordRegulationCheckBoxForSpecialChar = new CheckBox();
        passwordRegulationCheckBoxForSpecialChar.setText("!@#$%^&*");
        new HBox().setMargin(passwordRegulationCheckBoxForSpecialChar, new Insets(0, 10, 0, 0));
        passwordRegulationCheckBoxForCase = new CheckBox();
        passwordRegulationCheckBoxForCase.setText("A-Za-z");
        new HBox().setMargin(passwordRegulationCheckBoxForCase, new Insets(0, 10, 0, 0));
        passwordRegulationCheckBoxForNumber = new CheckBox();
        passwordRegulationCheckBoxForNumber.setText("0-9");
        HBox tempHBoxForPasswordRegulationCheckBox = new HBox();
        tempHBoxForPasswordRegulationCheckBox.getChildren().addAll(
                passwordRegulationCheckBoxForSpecialChar,
                passwordRegulationCheckBoxForCase,
                passwordRegulationCheckBoxForNumber
        );
        new VBox().setMargin(tempHBoxForPasswordRegulationCheckBox, new Insets(0, 0, 10, 0));

        Label remarkLabel = new Label();
        remarkLabel.setText("备注");
        remarkLabel.getStyleClass().add("detail-view-label");
        new VBox().setMargin(remarkLabel, new Insets(0, 10, 10, 0));

        remarkTextArea = new TextArea();
        remarkTextArea.setWrapText(true);
        remarkTextArea.getStyleClass().add("detail-view-text-area");
        new VBox().setMargin(remarkTextArea, new Insets(0, 0, 10, 0));

        warningLabel = new Label();
//        warningLabel.setText("操作失败");
        warningLabel.getStyleClass().addAll("detail-view-warning");
        VBox tempVBox = new VBox();
        tempVBox.getChildren().addAll(warningLabel);
        HBox.setHgrow(tempVBox, Priority.ALWAYS);

        saveButton = new Button();
        saveButton.setText("保存");
        HBox.setMargin(saveButton, new Insets(0, 10, 0, 0));
        cancelButton = new Button();
        cancelButton.setText("取消");
        HBox actionBar = new HBox();
        actionBar.getChildren().addAll(tempVBox, saveButton, cancelButton);

        getStyleClass().addAll("detail-view");
        getChildren().addAll(
                nameLabel,
                nameTextField,
                accountLabel,
                accountTextField,
                passwordLabel,
                tempHBoxForPassword,
                passwordLengthLabel,
                passwordLengthSlider,
                passwordRegulationLabel,
                tempHBoxForPasswordRegulationCheckBox,
                remarkLabel,
                remarkTextArea,
                actionBar
        );
        VBox.setVgrow(this, Priority.ALWAYS);
    }

    public TextField getNameTextField() {
        return nameTextField;
    }

    public TextField getAccountTextField() {
        return accountTextField;
    }

    public TextField getPasswordTextField() {
        return passwordTextField;
    }

    public Slider getPasswordLengthSlider() {
        return passwordLengthSlider;
    }

    public CheckBox getPasswordRegulationCheckBoxForSpecialChar() {
        return passwordRegulationCheckBoxForSpecialChar;
    }

    public CheckBox getPasswordRegulationCheckBoxForCase() {
        return passwordRegulationCheckBoxForCase;
    }

    public CheckBox getPasswordRegulationCheckBoxForNumber() {
        return passwordRegulationCheckBoxForNumber;
    }

    public TextArea getRemarkTextArea() {
        return remarkTextArea;
    }

    public Label getWarningLabel() {
        return warningLabel;
    }

    public Button getGenerationButton() {
        return generationButton;
    }

    public Button getSaveButton() {
        return saveButton;
    }

    public Button getCancelButton() {
        return cancelButton;
    }
}
