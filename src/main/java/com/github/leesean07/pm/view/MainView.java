package com.github.leesean07.pm.view;

import com.github.leesean07.pm.kit.AppProperty;
import com.github.leesean07.pm.kit.BeanManager;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class MainView extends VBox {
    public MainView() {
        getStyleClass().addAll("main-view");
    }

    public void show(Stage primaryStage) {
        AppProperty appProperty = BeanManager.getAppProperty();
        Scene scene = new Scene(this);
        scene.getStylesheets().add(getClass().getResource(appProperty.getMainStyle()).toExternalForm());
        primaryStage.getIcons().add(new Image(getClass().getResource(appProperty.getAppLogo()).toExternalForm()));
        primaryStage.setTitle(appProperty.getAppTitle());
        primaryStage.setScene(scene);
        primaryStage.setResizable(false);
        primaryStage.setWidth(appProperty.getMainWidth());
        primaryStage.setHeight(appProperty.getMainHeight());
        primaryStage.show();
    }
}
