package com.github.leesean07.pm;

import com.github.leesean07.pm.kit.BeanManager;
import javafx.application.Application;
import javafx.stage.Stage;

public class App extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        BeanManager.getMainController().initAndGetView().show(primaryStage);
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}
